# Install gitea and configuration
doc : https://docs.gitea.com/
source : https://www.youtube.com/watch?v=P4L9M-t1gTk

TOC
1. Update and upgrade OS
2. Create git user for gitea
4. Create gitea folder
5. Create docker-compose file
6. (optional) Add additional disk
7. Add https
9. Add ssh

## Update and upgrade OS
```
sudo apt update
sudo apt upgrade
```

## Create git user for gitea
```
sudo adduser --system --shell /bin/bash --gecos 'Git Version Control' --group --disabled-password --home /home/git git
```

## Create gitea folder 
```
mkdir ~/gitea
cd ~/gitea
```

## Create docker-compose.yml file
```
nano docker-compose.yml
```

copy code from gitea documentation
[install-with-docker](https://docs.gitea.com/installation/install-with-docker)

```
version: "3"

networks:
  gitea:
    external: false

services:
  server:
    image: gitea/gitea:1.19.3
    container_name: gitea
    environment:
      - USER_UID=1000
      - USER_GID=1000
    restart: always
    networks:
      - gitea
    volumes:
      - ./gitea:/data
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "3000:3000"
      - "222:22"
```


change server to gitea
```
...
services:
  gitea:
    image: gitea/gitea:1.19.3
...
```

get uid and gid for git user
```
id git
```

change the USER_UID, USER_GID to
```
...
      - USER_UID=113
      - USER_GID=120
...
```
change the port to
```
...
      - "127.0.0.1:3000:3000"
      - "127.0.0.1:2222:22"
...
```

give a name to gitea network
```
...
networks:
  gitea:
    name: gitea
...
```

add postgres db
```
...
gitea-db:
    image: postgres:alpine
    container_name: gitea-db
    restart: always
    volumes:
      - gitea-db:/var/lib/postgresql/data
    environment:
      - POSTGRES_DB=database
      - POSTGRES_USER=gitea
      - POSTGRES_PASSWORD=password
    networks:
      - gitea
...
```

add a docker volume for db
```
...
volumes:
  gitea-db: {}
...
```

link to gitea
```
gitea:
    ...
    environment:
      - USER_UID=113
      - USER_GID=120
      - APP_NAME=Gitea
      - DB_TYPE=postgres
      - DB_HOST=gitea-db:5432
      - DB_NAME=database
      - DB_USER=gitea
      - DB_PASSWD=password
...
```

## (optional) Add additional disk

add disk to vm instance from gcp

mount disk in vm instance
```
sudo mkfs.ext4 /dev/sdb
sudo mkdir /gitea
sudo mount /dev/sdb /gitea
```

see disk space
```
df
```

change data folder to /gitea from additional disk
```
services:
  gitea:
...
    volumes:
      - /gitea:/data
```

all the code
```
version: "3"

networks:
  gitea:
    name: gitea

volumes:
  gitea-db: {}

services:
  gitea:
    image: gitea/gitea:${GITEA_VERSION:-1.19.3}
    container_name: gitea
    environment:
      - USER_UID=113
      - USER_GID=120
      - APP_NAME=Gitea
      - DB_TYPE=postgres
      - DB_HOST=gitea-db:5432
      - DB_NAME=database
      - DB_USER=gitea
      - DB_PASSWD=password
    restart: always
    networks:
      - gitea
    volumes:
      - /gitea:/data
      - /home/git/.ssh/:/data/git/.ssh
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "127.0.0.1:3000:3000"
      - "127.0.0.1:2222:22"

  gitea-db:
    image: postgres:alpine
    container_name: gitea-db
    restart: always
    volumes:
      - gitea-db:/var/lib/postgresql/data
    environment:
      - POSTGRES_DB=database
      - POSTGRES_USER=gitea
      - POSTGRES_PASSWORD=password
    networks:
      - gitea
```

run docker-compose with detache option to run service in background
```
docker-compose up -d
```

## Add https
install nginx for reverse proxy
```
sudo apt install nginx
```

allow nginx in firewall
```
sudo ufw allow "Nginx Full"
```

create a config file for reverse-proxying gitea
```
sudo nano /etc/nginx/sites-available/gitea
```

https://docs.gitea.com/next/administration/reverse-proxies
copy the code below
```
server {
  server_name 34.79.254.107;

  root /var/www/html;

  location / {
    
    proxy_pass http://localhost:3000;

    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  }
}
```

create a link to enable gitea reverse-prox
```
sudo ln -s /etc/nginx/sites-available/gitea /etc/nginx/sites-enabled/gitea
```

test the configuration file
```
sudo nginx -t
```

restart nginx
```
sudo systemctl restart nginx
```

add ssh config

[sshing-shim-with-authorized_keys](https://docs.gitea.com/installation/install-with-docker#sshing-shim-with-authorized_keys)

create host ssh key gor gitea
```
sudo -u git ssh-keygen -t rsa -b 4096 -C "Gitea Host Key"
```

add host ssh key to authorized_keys
```
sudo -u git cat /home/git/.ssh/id_rsa.pub | sudo -u git tee -a /home/git/.ssh/authorized_keys
sudo -u git chmod 600 /home/git/.ssh/authorized_keys
```

create the fake host gitea command that will forward commands from the host to the container
```
cat <<"EOF" | sudo tee /usr/local/bin/gitea
#!/bin/sh
ssh -p 2222 -o StrictHostKeyChecking=no git@127.0.0.1 "SSH_ORIGINAL_COMMAND=\"$SSH_ORIGINAL_COMMAND\" $0 $@"
EOF
```
```
sudo chmod +x /usr/local/bin/gitea
```

# Run command inside container

open bash session in container
```
docker exec --user git -it gitea bash
gitea --help
```

# Add HTTPS

update and install certbot
```
sudo apt update
sudo apt install certbot
```
stop nginx service
```
sudo systemctl stop nginx
```

create certificate with certbot
```
sudo certbot certonly -d your.domain.com
# choose 1
# add your email
# choose a
# choose y
```

modif gitea config for https
```
sudo nano /etc/nginx/sites-enabled/gitea
```

```
server {
  listen 80;
  listen [::]:80;
  server_name your.domain.com;
  return 301 https://$host$request_uri;
}
server {
    listen 443 ssl;
    listen [::]:443 ssl;
    server_name your.domain.com;
    index   index.php;
    location ^~ / {
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header Host $host;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass http://127.0.0.1:3000;
      proxy_read_timeout 90;
    }
    ssl_certificate /etc/letsencrypt/live/your.domain.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/your.domain.com/privkey.pem; # managed by Certbot
}
```

test nginx conf
```
sudo nginx -t
```

restart nginx
```
sudo systemctl start nginx
```

try https://your.domain.com/

change your app.ini file:
```
sudo nano /gitea/gitea/conf/app.ini

[server]
APP_DATA_PATH    = /data/gitea
DOMAIN           = your.domain.com                           
SSH_DOMAIN       = your.domain.com                           
HTTP_PORT        = 3000
ROOT_URL         = https://your.domain.com
```

# restrict registration to admin only

change your app.ini file:
```
sudo nano /gitea/gitea/conf/app.ini

[service]
# Change this
DISABLE_REGISTRATION              = true
REQUIRE_SIGNIN_VIEW               = true
# Add this to only allow youremail domains
EMAIL_DOMAIN_WHITELIST            = your.domain.com
```

# Dump gitea

create a gitea-dump folder
```
cd /gitea
sudo mkdir gitea-dump
sudo chmod -R 777 gitea-dump
```

run gitea dump
```
docker exec -u git -it -w /data/gitea-dump gitea bash -c '/usr/local/bin/gitea dump -c /data/gitea/conf/app.ini'
```
or inside the conainer
```
docker exec --user git -it gitea bash
gitea dump -c /data/gitea/conf/app.ini -w /data/gitea-dump
```

download from gcp
```
instance_name=
zip_name=
zone
gcloud compute scp ${instance_name}:/gitea/gitea-dump/${zip_name}.zip . --project ${project_id} --zone ${zone}
```

# Add lfs
[git-lfs-setup](https://docs.gitea.com/administration/git-lfs-setup)
```
docker exec --user git -it gitea bash
nano /data/gitea/conf/app.ini

[server]
; Enables git-lfs support. true or false, default is false.
LFS_START_SERVER = true

[lfs]
; Where your lfs files reside, default is data/lfs.
PATH = /data/git/lfs
```

# migration

after a dump you can migrate data in other instance
```
# open bash session in container
docker exec --user git -it gitea bash
cd /data/gitea-dump/
sudo unzip gitea-dump-*.zip -d backup
cd backup/
# restore the gitea data
mv data/* /data/gitea
# restore the repositories itself
mv repos/* /data/git/repositories/
# adjust file permissions
chown -R git:git /data
# Regenerate Git Hooks
/usr/local/bin/gitea -c '/data/gitea/conf/app.ini' admin regenerate hooks
```

database migration

```
# postgres
psql -U $USER -d $DATABASE < gitea-db.sql
```

## Add drone ci

add drone to applicaton
https://git.ia.avec.fr/admin/applications/oauth2

get ID du client and Secret du client

URL de redirection
https://drone.ia.avec.fr/login

```
volumes:
  gitea-db:
  drone:
  drone-agent:

drone:
    container_name: drone
    image: drone/drone:${DRONE_VERSION:-2.17.0}
    restart: unless-stopped
    depends_on:
      - gitea
    environment:
      # https://docs.drone.io/server/provider/gitea/
      - DRONE_DATABASE_DRIVER=postgres
      - DRONE_DATABASE_DATASOURCE=postgres://gitea:password@gitea-db:5432/postgres?sslmode=disable
      - DRONE_GITEA_SERVER=https://git.ia.avec.fr
      - DRONE_RPC_SECRET=${DRONE_RPC_SECRET:-9c3921e3e748aff725d2e16ef31fbc42}
      - DRONE_SERVER_PROTO=https
      - DRONE_SERVER_HOST=drone.ia.avec.fr
      - DRONE_TLS_AUTOCERT=false
      - DRONE_USER_CREATE="username:${GITEA_ADMIN_USER:-maxime},machine:false,admin:true,token:${DRONE_RPC_SECRET:-9c3921e3e748aff725d2e16ef31fbc42}"
      - DRONE_GITEA_CLIENT_ID=${DRONE_GITEA_CLIENT_ID:-707937ea-2825-4945-b08a-70dff1d7ef42}
      - DRONE_GITEA_CLIENT_SECRET=${DRONE_GITEA_CLIENT_SECRET:-gto_iutpwv7p5v3yk36tc7c25votouz5hkngqugpyjaoaqa3mppoilcq}
      - DRONE_GIT_ALWAYS_AUTH=true
    ports:
      - "127.0.0.1:3001:80"
      - "127.0.0.1:9000:9000"
    networks:
      - gitea
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /gitea/drone:/data
  
  drone-runner:
    container_name: drone-runner
    image: drone/drone-runner-docker:${DRONE_RUNNER_VERSION:-1.8.3}
    restart: unless-stopped
    depends_on:
      - drone
    environment:
      # https://docs.drone.io/runner/docker/installation/linux/
      # https://docs.drone.io/server/metrics/
      - DRONE_RPC_PROTO=https
      - DRONE_RPC_HOST=drone.ia.avec.fr
      - DRONE_RPC_SECRET=${DRONE_RPC_SECRET:-9c3921e3e748aff725d2e16ef31fbc42}
      - DRONE_RUNNER_NAME=gitea-runner
      - DRONE_RUNNER_CAPACITY=2
      - DRONE_RUNNER_NETWORKS=gitea
      - DRONE_DEBUG=false
      - DRONE_TRACE=false
    ports:
      - "127.0.0.1:3002:3000"
    networks:
      - gitea
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
```

docker compose up -d

go to https://drone.ia.avec.fr/

authorize app

skip register and go to https://drone.ia.avec.fr/

sync and active repository
